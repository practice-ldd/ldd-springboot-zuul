package springcloud.service.ribbon.config;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Auther: liudong
 * @Date: 18-11-1 16:21
 * @Description:
 */
@Component
@Slf4j
public class MyFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.debug("%s >>> %s", request.getMethod(), request.getRequestURL().toString());
        String token = request.getHeader("token");
        if (!ifRight(token)){
            //认证失败
            log.error("token验证失败");
            HttpServletResponse response = ctx.getResponse();
            response.setCharacterEncoding("utf-8");  //设置字符集
            response.setContentType("text/html; charset=utf-8"); //设置相应格式
            response.setStatus(401);
            ctx.setSendZuulResponse(false); //不进行路由
            try {
                response.getWriter().write("token 验证失败"); //响应体
            } catch (IOException e) {
                log.error("response io异常");
                e.printStackTrace();
            }
            ctx.setResponse(response);
            return null;
        }
        log.info("token验证成功");
        return null;
    }

    private boolean ifRight(String token) {
        return false;
    }

}
